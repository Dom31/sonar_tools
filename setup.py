# Always prefer setuptools over distutils
from setuptools import setup, find_packages
# To use a consistent encoding
from codecs import open
from os import path
import os.path
#

import sys


# run under Windows OS: python setup.py install --user --prefix=check
# ensure python verison > 3.x (PATH in system variables: Search: System -> Advanced System Settings -> Environment Variables
# Path -> Edit : suppress Python 2.x path and add/confirm Python 3 path
# sgs_tool_access.json under  %HOMEPATH%
PYTHON_INSTALL_DIR = os.path.dirname(os.path.dirname(os.__file__))
os.environ['TCL_LIBRARY'] = os.path.join(PYTHON_INSTALL_DIR, 'tcl', 'tcl8.6')
os.environ['TK_LIBRARY'] = os.path.join(PYTHON_INSTALL_DIR, 'tcl', 'tk8.6')

base = None

if sys.platform=='win32':
    base = "Win32GUI"

icone = None
if sys.platform == "win32":
    icone = "icone.ico"
    import cx_Freeze
    executables = [cx_Freeze.Executable("metricsGUI/checkMetrics_gui.py"),cx_Freeze.Executable("metrics/checkMetrics.py")]

    cx_Freeze.setup(
            name = "get_metrics from_SonarQube",
            options = {"build_exe":{
                "packages":["tkinter","re","pygubu"],
                'include_files':[
                    os.path.join(PYTHON_INSTALL_DIR, 'DLLs', 'tk86t.dll'),
                    os.path.join(PYTHON_INSTALL_DIR, 'DLLs', 'tcl86t.dll'),
                    'metrics/logging.yaml',
                    'metricsGUI/logging_gui.yaml',
                    'metricsGUI/sonar_gui.ui',
                    'metrics/sgs_tool_access.json'
                ],
                "zip_include_packages": "*",
                "zip_exclude_packages": "",
            }
                       },
            version="0.01",
    executables=executables)


here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()


setup(
    name='checkMetrics',
    version='1.0.0',
    description='A python package for getting metrics in sonarqube server',
    long_description=long_description,
    long_description_content_type="text/markdown",
    license="EUCLID LPEG 2",
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Natural Language :: English',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3'
    ],
    keywords='python package template documentation testing continuous deployment',
    packages=find_packages(exclude=['docs', 'tests','data','build','htmlcov']),
    package_data = { 'metrics' : ['*.yaml', '*.json', '*.ui'] },
    setup_requires=['pytest-runner', 'cx_Freeze','pygubu','m2r','openpyxl<2.6.0','setuptools>=38.6.0'],  # >38.6.0 needed for markdown README.md
    tests_require=['pytest', 'pytest-cov'],
    entry_points={
        'console_scripts': [
            'checkMetrics=metrics.checkMetrics:main',
            'analyzeMetrics=metrics.analyzeMetrics:main'
        ],
    }
)



