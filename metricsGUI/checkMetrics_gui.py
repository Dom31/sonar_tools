# encoding: utf8
# peopleGUI.py
import sys
import os
import json
import logging
import logging.config
import tkinter as tk
from tkinter import messagebox, filedialog
from yaml import safe_load
import pygubu

# logger = logging.getLogger(__name__)
from metrics import checkMetrics
from metricsGUI import console_gui

"""

GUI app for people...

TODO: add docstrings header
"""


def filename_with_full_path (filename):
    """
    full path/file whaever the OS
    __file__ is not recognized in Windows (pb with cx_freeze)

    Parameters
    ----------
    filename: str : short file name

    Returns
    -------
    str
        path/filename
    """
    if getattr(sys, 'frozen', False):
        # The application is frozen
        current_dir = os.path.dirname(sys.executable)
    else:
        # The application is not frozen
        # Change this bit to match where you store your data files:
        current_dir = os.path.dirname(__file__)

    return os.path.join(current_dir, filename)

class MyApplication(pygubu.TkApplication):
    def _create_ui(self):
        # 1: Create a builder
        self.builder = builder = pygubu.Builder()

        # 2: Load an ui file
        # menu_ui_file = 'sonar_gui.ui'
        menu_ui_file = filename_with_full_path('sonar_gui.ui')
        builder.add_from_file(menu_ui_file)

        # 3: Create the widget using self.master as parent
        self.mainwindow = mainwindow = self.top3 = builder.get_object("mainwindow", self.master)

        # Set main menu
        self.mainmenu = menu = builder.get_object("mainmenu", self.master)
        self.set_menu(menu)



        # change main window message
        label = self.builder.get_object('label_main')
        console_frame = self.builder.get_object('label_main')
        self.console = console_gui.ConsoleUi(console_frame)

        #formatter = logging.Formatter('%(asctime)s: %(message)s')
        formatter_str = logger.handlers[0].formatter._fmt
        formatter = logging.Formatter(formatter_str)
        self.console.queue_handler.setFormatter(formatter)
        logger.addHandler(self.console.queue_handler)


        # Initialize model
        # Here just try a connextion (and possibly raise an exception)
        with open(checkMetrics.CONF_DEFAULT_FILE) as json_file:
            data = json.load(json_file)
        logger.info("SonarQube URL     : {}".format(data['sonar']['url']))
        logger.info("SonarQube Username: {}".format(data['sonar']['username']))

        checkMetrics.try_connexion_to_sonarQube(checkMetrics.CONF_DEFAULT_FILE)

        # Configure callbacks
        builder.connect_callbacks(self)

    def on_mSonar_item_clicked(self, itemid):
        if itemid == "mSonar_open":
            input_file = filedialog.askopenfilename(initialdir=".", title="Select SonarQube access configuration file",
                                                    filetypes=(("json files", "*.json"), ("all files", "*.*")))
            logger.info("File = {}".format(input_file))

            # TODO : avoid this global variable re-setting...
            global CONF_DEFAULT_FILE
            checkMetrics.CONF_DEFAULT_FILE=input_file
            checkMetrics.try_connexion_to_sonarQube(input_file)
            version, status = checkMetrics.get_sonar_status_and_version(input_file)
            logger.info("sonarQube server version: {}".format(version))
            logger.info("sonarQube server status: {}".format(status))

        if itemid == "mSonar_access":
            version, status = checkMetrics.get_sonar_status_and_version(checkMetrics.CONF_DEFAULT_FILE)
            logger.info("sonarQube server version: {}".format(version))
            logger.info("sonarQube server status: {}".format(status))


        if itemid == "mSonar_quit":
            # messagebox.showinfo("File", "By by !")
            self.quit()



    def on_mProjects_item_clicked(self, itemid):
        if itemid == "mProjects_list":
            logger.info("list of the projects:")

            projects = checkMetrics.get_projects(checkMetrics.CONF_DEFAULT_FILE, add_periods=True)
            checkMetrics.write_sonarQube_data(projects, False, "sonarQube_projects.xlsx", section_name="sonarQube projects")




    def on_mdata_item_clicked(self, itemid):
        if itemid == "mdata_metrics_available":
            logger.info("Getting the list of the available metrics in SonarQube server ...")
            #logger.info(get_metrics_from_sonar.COMPONENT_METRICS)
            metrics_available = checkMetrics.get_metrics_available_in_sonarQube(checkMetrics.CONF_DEFAULT_FILE);
            checkMetrics.write_sonarQube_data(metrics_available, False, "sonarQube_metrics_definitions.xlsx", section_name="Metrics Definitions")
            logger.info("Task done.")

        if itemid == "mdata_metrics_values":
            logger.info("Metrics values...")

    def on_about_clicked(self):
        messagebox.showinfo("About", "sonarMetrics V0.1 (prototype)")

    def on_help_clicked(selfself):

        logger.info("Usage and help in the current console...")
        parser = checkMetrics.construct_parser()
        text = parser.print_help()
        #logger.info( parser.print_help())


if __name__ == "__main__":

    log_file = filename_with_full_path("logging_gui.yaml")
    try:
        with open(log_file) as handle:
            log_cfg = safe_load(handle)
    except Exception as e:
        raise ValueError('Could not load loogging data file: {0}'.format(e))

    logging.config.dictConfig(log_cfg)
    # create (root) logger
    logger = logging.getLogger()
    # logger.setLevel(logging.DEBUG)
    logger.info("Hello in terminal")
    logger.debug("START __main__")

    root = tk.Tk()
    app = MyApplication(root)
    app.run()
    logger.debug("END __main__")
