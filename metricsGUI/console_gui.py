import logging
import queue

from tkinter.scrolledtext import ScrolledText
from tkinter import Scrollbar,ttk,Text

from metricsGUI import queue_handler
from tkinter import N, S, E, W
import tkinter as tk

logger = logging.getLogger(__name__)


class ConsoleUi():
    """Poll messages from a logging queue and display them in a scrolled text widget"""

    def __init__(self,frame):
        #self.logger= logger
        self.frame = frame
        # Create a ScrolledText wdiget

        self.text = Text(frame,state='disabled', width = 200,height=30)
        vs = Scrollbar(frame, orient="vertical")
        hs = Scrollbar(frame, orient="horizontal")
        sizegrip = ttk.Sizegrip(frame)

        self.text.configure(font='TkFixedFont')
        self.text.tag_config('INFO', foreground='black')
        self.text.tag_config('DEBUG', foreground='gray')
        self.text.tag_config('WARNING', foreground='orange')
        self.text.tag_config('ERROR', foreground='red')
        self.text.tag_config('CRITICAL', foreground='red', underline=1)

        # hook up the scrollbars to the text widget
        self.text.configure(yscrollcommand=vs.set, xscrollcommand=hs.set, wrap="none")
        vs.configure(command=self.text.yview)
        hs.configure(command=self.text.xview)

        # grid everything on-screen
        self.text.grid(row=0, column=0, sticky="news")
        vs.grid(row=0, column=1, sticky="ns")
        hs.grid(row=1, column=0, sticky="news")
        sizegrip.grid(row=1, column=1, sticky="news")
        frame.grid_rowconfigure(0, weight=1)
        frame.grid_columnconfigure(0, weight=1)


        # Create a logging handler using a queue
        self.log_queue = queue.Queue()
        self.queue_handler = queue_handler.QueueHandler(self.log_queue)
        formatter = logging.Formatter('%(asctime)s: %(message)s')
        self.queue_handler.setFormatter(formatter)

        # Start polling messages from the queue
        self.frame.after(100, self.poll_log_queue)

    def display(self, record):
        msg = self.queue_handler.format(record)
        #self.scrolled_text.configure(state='normal')
        #self.scrolled_text.insert(tk.END, msg + '\n', record.levelname)
        #self.scrolled_text.configure(state='disabled')

        self.text.configure(state='normal')
        self.text.insert(tk.END, msg + '\n', record.levelname)
        self.text.configure(state='disabled')

        # Autoscroll to the bottom
        self.text.yview(tk.END)

    def poll_log_queue(self):
        # Check every 100ms if there is a new message in the queue to display
        while True:
            try:
                record = self.log_queue.get(block=False)
            except queue.Empty:
                break
            else:
                self.display(record)
        self.frame.after(100, self.poll_log_queue)