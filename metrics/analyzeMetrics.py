#
# Copyright (C) 2012-2020 Euclid Science Ground Segment
#
# This library is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser General
# Public License as published by the Free Software Foundation; either version 3.0 of the License, or (at your option)
# any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License along with this library; if not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
#

"""
Analyze:
- the metrics stored in sonarQube for a given projet or set of projects

sonarQube:
# https://docs.sonarqube.org/7.9/user-guide/metric-definitions/

"""

import argparse
import signal
import sys
import os

signal.signal(signal.SIGINT, lambda x, y: sys.exit(0))

from yaml import safe_load
import logging.config
import logging.handlers

logger = logging.getLogger(__name__)

from input_output import dictionaries, excel_sheet, file_access
import pandas as pd
import plotnine as p9
from mizani.formatters import percent_format
import warnings
from matplotlib.backends.backend_pdf import PdfPages

warnings.filterwarnings('ignore', module='plotnine')

SCRIPT_VERSION = "0.9"
ALL_METRIC_DOMAINS = ["project_summary", "duplications", 'sizes', 'issues']


def analyze_issues (project_name, project_directory, df_metrics, df_issues):
    """
     analyse issues
    :param project_name: name of the project analyzed
    :param project_directory: directory for input and output files
    :param df_metrics: metrics pandas data frames
    :param df_issues: issues pandas data frames
    :return:
    """
    print("analyze_issues")

    analysis_output_file = project_directory + "/issues/analysis_issues.txt"
    issues_severities_image = project_directory + "/issues/issues_severities_image.png"
    issues_types_image = project_directory + "/issues/issues_type_image.png"
    issues_code_smells_image = project_directory + "/issues/issues_code_smells_image.png"
    issues_code_smells_ncloc_image = project_directory + "/issues/issues_code_smells_ncloc_image.png"

    critical_blocker_issues_file = project_directory + "/issues/critical_blocker_issues.csv"
    major_bugs_file = project_directory + "/issues/major_bugs.csv"
    major_vulnerabilities_file = project_directory + "/issues/major_vulnerabilities.csv"
    major_code_smells_file = project_directory + "/issues/major_code_smells.csv"
    major_code_smells_by_project_file = project_directory + "/issues/major_code_smells_by_project.csv"
    count_blocker_critical_issues_max = 0
    count_major_bugs_max = 0
    count_major_vulnerabilities_max = 0
    count_smells_by_100_ncloc_max = 5 # 5 issues max for 100 lines of code


    df = df_issues[(df_issues['status'] != 'CLOSED')]

    df_blocker_critical = df[(df['severity'] == 'CRITICAL') | (df['severity'] == 'BLOCKER')]
    df_issues_major = df[(df['severity'] == 'MAJOR')]
    df_bugs_major = df[(df['severity'] == 'MAJOR') & (df['type'] == 'BUG')]
    df_vulnerabilities_major = df[(df['severity'] == 'MAJOR') & (df['type'] == 'VULNERABILITY')]
    df_code_smells_major = df[(df['severity'] == 'MAJOR') & (df['type'] == 'CODE_SMELL')]

    dodge_text = p9.position_dodge(width=0.9)

    df_plot = (p9.ggplot(data=df, mapping=p9.aes(x='severity',fill='type'))
               + p9.geom_histogram(binwidth=1)
               + p9.labs(x='severity',y='issues', title='Number of issues by severity'))
    df_plot.save(issues_severities_image)
    print("output image created:", issues_severities_image)

    df_plot = (p9.ggplot(data=df_issues_major, mapping=p9.aes(x='type',fill='rule'))
               + p9.geom_histogram(binwidth=1)
               + p9.labs(x='issue type (severity = MAJOR)', y='Count', title='Number of MAJOR issues by type'))
    df_plot.save(issues_types_image)
    print("output image created:", issues_types_image)

    df_plot = (p9.ggplot(data=df_code_smells_major, mapping=p9.aes(x='project'))
               + p9.geom_histogram()
               + p9.labs(x='File', y='Count', title='Number of MAJOR code smells by SonarQube project'))
    df_plot.save(issues_code_smells_image)

    print("output image created:", issues_code_smells_image)


    export_csv = df_blocker_critical.to_csv(critical_blocker_issues_file, index=None, header=True)
    print(export_csv)
    print("output file created:", critical_blocker_issues_file)
    #
    export_csv = df_bugs_major.to_csv(major_bugs_file, index=None, header=True)
    print(export_csv)
    print("output file created:", major_bugs_file)

    export_csv = df_vulnerabilities_major.to_csv(major_vulnerabilities_file, index=None, header=True)
    print(export_csv)
    print("output file created:", major_vulnerabilities_file)

    export_csv = df_code_smells_major.to_csv(major_code_smells_file, index=None, header=True)
    print(export_csv)
    print("output file created:", major_code_smells_file)

    print ("Count the number of rules  by project")
    #df1 = df_code_smells_major.groupby(['component','rule']).size().reset_index(name='counts')
    #df_code_smells_major = df_code_smells_major[(df_code_smells_major['count'] > 10)]

    component_by_rules = df_code_smells_major.groupby('project')['rule'].count().to_frame('major_smells_counts').reset_index()
    #print(component_by_rules)
    #print(component_by_rules.columns)

    df_metrics = df_metrics[df_metrics['qualifier'] == 'TRK']
    df_metrics  = df_metrics[['key','ncloc']]
    component_by_rules_ncloc = pd.merge(left=component_by_rules, right=df_metrics, left_on='project', right_on='key')
    print(component_by_rules_ncloc)
    print(component_by_rules_ncloc.columns)
    component_by_rules_ncloc['count_smells_by_100_ncloc'] = component_by_rules_ncloc.apply(lambda row: 100 * (row.major_smells_counts / row.ncloc), axis=1)
    print(component_by_rules_ncloc)
    print(component_by_rules_ncloc.columns)

    df_plot = (p9.ggplot(data=component_by_rules_ncloc, mapping=p9.aes(x='component_by_rules_ncloc.index',y='count_smells_by_100_ncloc'))
                + p9.geom_point(alpha=0.5, size=2)
               + p9.geom_hline(p9.aes(yintercept=5), linetype='dashed')
               + p9.labs(x='projects',y='count_smells_by_100_ncloc', title='Number of major_smells by project (for 100 lines of code)'))
    df_plot.save(issues_code_smells_ncloc_image)
    print("output image created:", issues_code_smells_ncloc_image)

    component_by_rules_ncloc_nok = component_by_rules_ncloc[(component_by_rules_ncloc['count_smells_by_100_ncloc'] > count_smells_by_100_ncloc_max)]

    export_csv = component_by_rules_ncloc_nok.to_csv(major_code_smells_by_project_file, index=None, header=True)
    print(export_csv)
    print("output file created:", major_code_smells_by_project_file)

    count_blocker_critical_issues = df_blocker_critical.key.count()
    count_major_bugs_issues = df_bugs_major.key.count()
    count_df_code_smells_major = df_code_smells_major.key.count()
    count_major_vulnerabilities_issues = df_vulnerabilities_major.key.count()
    count_component_by_rules_ncloc_nok = component_by_rules_ncloc_nok.key.count()
    count_projects = df_metrics.key.count()


    count_issues = df.key.count()

    # count_too_long_files = filterinfDataframe.key.count()
    #
    with open(analysis_output_file, "w") as f:
        print(
            " {0:} issues ({1:4.1f}% of the total count of issues:{2:}) have  count of CRITICAL BLOCKER severities> MAX = {3:}".format(
                count_blocker_critical_issues, float(100 * count_blocker_critical_issues / count_issues), count_issues,
                count_blocker_critical_issues_max), file=f)

        print(
            " {0:} bugs ({1:4.1f}% of the total count of issues:{2:}) have  count of MAJOR severity> MAX = {3:}".format(
                count_major_bugs_issues, float(100 * count_major_bugs_issues / count_issues), count_issues,
                count_major_bugs_max), file=f)

        print(
            " {0:} vulnerabilities ({1:4.1f}% of the total count of issues:{2:}) have  count of MAJOR severity> MAX = {3:}".format(
                count_major_vulnerabilities_issues, float(100 * count_major_vulnerabilities_issues / count_issues),
                count_issues,
                count_major_vulnerabilities_max), file=f)
        print(
            " {0:} SonarQube projects  ({1:4.1f}% of the total count of projects :{2:}) have a count of major smells for 100 lines of code > MAX = {3:}".format(
                count_component_by_rules_ncloc_nok, float(100 * count_component_by_rules_ncloc_nok / count_projects),
                count_projects,
                count_smells_by_100_ncloc_max), file=f)

    print("output file created:", analysis_output_file)

def analyze_sizes(project_name, project_directory, df_metrics):
    """
     analyse sizes
    :param project_name: name of the project analyzed
    :param project_directory: directory for input and output files
    :param df_metrics: metrics pandas data frames
    :return:
    """
    print("analyze_sizes")

    analysis_output_file = project_directory + "/sizes/analysis_sizes.txt"
    file_lines_count_image = project_directory + "/sizes/file_lines_count_image.png"
    files_with_too_many_lines = project_directory + "/sizes/files_with_too_many_lines.csv"

    count_lines_by_file_max = 1000

    df2 = df_metrics[df_metrics['qualifier'] == 'FIL']
    df2 = df2[['key', 'lines', 'ncloc', 'language']]

    df_plot = (p9.ggplot(data=df2, mapping=p9.aes(x='ncloc', fill='language'))
               + p9.geom_histogram(binwidth=100)
               + p9.geom_vline(p9.aes(xintercept=1000), linetype='dashed')
               + p9.annotate('text', x=10, y=50, label="MAX = {}".format(count_lines_by_file_max))
               + p9.scale_x_continuous(limits=(0, 2000))
               + p9.labs(y='Count of files ', x='Count of lines of code', title='Number of files by lines of code'))
    #                 + p9.xlim(0, 2000)
    df_plot.save(file_lines_count_image)
    print("output image created:", file_lines_count_image)

    filterinfDataframe = df2[(df2['ncloc'] > count_lines_by_file_max)]

    export_csv = filterinfDataframe.to_csv(files_with_too_many_lines, index=None, header=True)
    print(export_csv)
    print("output file created:", files_with_too_many_lines)

    count_files = df2.key.count()
    count_too_long_files = filterinfDataframe.key.count()

    with open(analysis_output_file, "w") as f:
        print(" {0:} files ({1:4.1f}% of the total count of files:{2:}) have  count of lines of code > {3:}".format(
            count_too_long_files, float(100 * count_too_long_files / count_files), count_files,
            count_lines_by_file_max), file=f)
    print("output file created:", analysis_output_file)

def analyze_duplications(project_name, project_directory, df_metrics):
    """
     analyse duplications
    :param project_name: name of the project analyzed
    :param project_directory: directory for input and output files
    :param df_metrics: metrics pandas data frames
    :return:
    """
    print("analyze_duplications")

    analysis_output_file = project_directory + "/duplications/analysis_duplications.txt"
    duplicated_lines_density_image = project_directory + "/duplications/duplicated_lines_density.png"
    files_with_too_many_duplications_file = project_directory + "/duplications/files_with_too_many_duplications_file.csv"

    duplicated_lines_density_max = 5

    # df.to_csv(output_file, index=False)
    # head_data = df.head()
    # print ("data = ",df.dtypes)
    # print ("data = ",df.columns)
    df2 = df_metrics[df_metrics['qualifier'] == 'FIL']

    grouped_data_language = df2.groupby('language')
    # print("data = ", grouped_data_language['duplicated_lines_density'].describe())
    # print("data = ", grouped_data_language['duplicated_lines_density'].mean())

    df2 = df2[['key', 'duplicated_lines_density', 'language']]
    # print("data = ", df2)

    # Refer to a column in a dataframe

    dodge_text = p9.position_dodge(width=0.9)

    df_plot = (p9.ggplot(data=df2, mapping=p9.aes(x='duplicated_lines_density', fill='language'))
               + p9.geom_histogram(binwidth=3)
               + p9.geom_vline(p9.aes(xintercept=5), linetype='dashed')
               + p9.annotate('text', x=10, y=50, label="MAX = {}".format(duplicated_lines_density_max)))

    #              + p9.geom_text(p9.aes(label='stat(prop)*100'), stat='count', nudge_y=0.125,va='bottom',format_string='{:.1f}% ')
    # df_plot=(p9.ggplot(data=df2, mapping=p9.aes(x='df2.index', y='duplicated_lines_density', fill='df2.index')) +
    # p9.geom_boxplot()
    # )

    # df_plot=(p9.ggplot(data=df2, mapping=p9.aes(x='df2.index', y='duplicated_lines_density'))
    #         + p9.geom_point())
    # + p9.geom_point()
    # geom_histogram(binwidth = 3)

    # df_plot =p9.ggplot(data=df2,mapping=p9.aes(x='df2.index', y='duplicated_lines_density'))
    # df_plot + p9.geom_point()
    df_plot.save(duplicated_lines_density_image)
    print("output image created:", duplicated_lines_density_image)

    filterinfDataframe = df2[(df2['duplicated_lines_density'] > duplicated_lines_density_max)]
    # print ("functions with duplicated_lines_density > {}%".format(duplicated_lines_density_max))
    # print (filterinfDataframe)

    export_csv = filterinfDataframe.to_csv(files_with_too_many_duplications_file, index=None, header=True)
    print(export_csv)
    print("output file created:", files_with_too_many_duplications_file)

    # p9.save_as_pdf_pages([df_plot],'duplicated_lines_density.pdf')
    count_files = df2.key.count()
    dupl_count_files = filterinfDataframe.key.count()

    with open(analysis_output_file, "w") as f:
        print(" {0:} files ({1:4.1f}% of the total count of files:{2:}) have  duplicated_lines_density > {3:}%".format(
            dupl_count_files, float(100 * dupl_count_files / count_files), count_files, duplicated_lines_density_max),
            file=f)
    print("output file created:", analysis_output_file)


def analyze_project_summary(project_name,project_directory,df_metrics):
    """

    :param project_name: name of the project analyzed
    :param project_directory: directory for input and output files
    :param df_metrics: metrics pandas data frames
    :return:
    """
    print("project_summary")

    analysis_output_file = project_directory + "/project_summary_file.txt"

    df_projects = df_metrics[df_metrics['qualifier'] == 'TRK']
    df_projects_files = df_metrics[df_metrics['qualifier'] == 'FIL']
    df_projects_files_cpp = df_projects_files[df_projects_files['language'] == 'c++']
    df_projects_files_py = df_projects_files[df_projects_files['language'] == 'py']

    count_projects = df_projects.id.count()
    count_directories = df_metrics[df_metrics['qualifier'] == 'DIR'].id.count()
    count_files = df_projects_files.id.count()
    count_classes = int(df_projects['classes'].sum())
    count_functions = int(df_projects['functions'].sum())
    # count_lines_of_code = int(df_projects['ncloc'].sum())
    count_lines_of_code_cpp = int(df_projects_files_cpp['ncloc'].sum())
    count_lines_of_code_py = int(df_projects_files_py['ncloc'].sum())
    count_lines_of_code_cpp_py = count_lines_of_code_cpp + count_lines_of_code_py

    with open(analysis_output_file, "w") as f:
        print("Count of SonarQube projects: {}".format(count_projects), file=f)
        print("Count of directories: {0:} ({1:4.1f} directories by project)".format(count_directories, float(
            count_directories / count_projects)), file=f)
        print("Count of files: {0:} ({1:4.1f} files by directory)".format(count_files, count_files / count_directories),
              file=f)
        print("Count of classes: {0:} ({1:4.1f} classes by file)".format(count_classes, count_classes / count_files),
              file=f)
        print("Count of functions: {0:} ({1:4.1f} functions by class)".format(count_functions,
                                                                              count_functions / count_classes), file=f)
        print("Count of ncloc C++ & Python: {0:}  (C++:{1:} lines:{2:4.1f}% and Python:{3:} lines:{4:4.1f}%)". \
              format(count_lines_of_code_cpp_py, count_lines_of_code_cpp,
                     count_lines_of_code_cpp * 100 / count_lines_of_code_cpp_py, count_lines_of_code_py,
                     count_lines_of_code_py * 100 / count_lines_of_code_cpp_py), file=f)

    # print(df_projects.columns)
    print("output file created:", analysis_output_file)


def get_data_frames(project_name, project_working_directory):
    """
    read the input Excel files and read the pandas dataframes
    :param project_name: name of the project analyzed
    :param project_working_directory: directory for input and output files
    :return: pandas dataframes: metrics and issues
    """

    input_metrics_file = project_working_directory + "/metrics_" + project_name + ".xlsx"
    input_issues_file = project_working_directory + "/issues_" + project_name + ".xlsx"

    print ("metrics file : ",input_metrics_file)
    print ("issues files: ", input_issues_file)
    # open input ExcelSheet
    df_metrics = pd.read_excel(input_metrics_file, sheet_name=0)
    df_issues = pd.read_excel(input_issues_file, sheet_name=0)

    return df_metrics, df_issues


def construct_parser():
    parser = argparse.ArgumentParser()

    parser.add_argument("-m",
                        "--metric_domains",
                        action="store",
                        nargs="*",
                        metavar="NAME",
                        default="project_summary",
                        help="A space separated list of metrics names to be stored in the output (default all)")

    parser.add_argument("-q",
                        "--quiet",
                        action="store_true",
                        help="no output on the screen")

    parser.add_argument("-r",
                        "--project_report_path",
                        action="store",
                        metavar="DIR",
                        default="/home/user/Documents/Reports",
                        help="input and output directory. Last directory is the project name (default='/home/user/Documents/Reports/NewSonar7.9)")

    parser.add_argument("-v",
                        "--version",
                        action="store_true",
                        help="Print the version of this script")

    return parser


def main(args=None):
    """The main routine."""
    if args is None:
        args = sys.argv[1:]

    log_file = file_access.filename_with_full_path(__file__, "logging.yaml")

    try:
        with open(log_file) as handle:
            log_cfg = safe_load(handle)
    except Exception as e:
        raise ValueError('Could not load logging configuration file: {0}'.format(e))

    logging.config.dictConfig(log_cfg)
    # create (root) logger
    logger = logging.getLogger()
    # logger.setLevel(logging.DEBUG)
    logger.debug("START __main__")

    parser = construct_parser()
    args = parser.parse_args()

    if args.version:
        logger.info("version {}".format(SCRIPT_VERSION))
    else:

        project_directory = args.project_report_path
        upper_dir, project_name = os.path.split(project_directory)

        df_metrics, df_issues = get_data_frames(project_name, project_directory)

        if args.metric_domains[0] == "all":
            args.metric_domains = ALL_METRIC_DOMAINS
        for metric_domain in args.metric_domains:
            print(metric_domain)
            if not metric_domain in ALL_METRIC_DOMAINS:
                raise ValueError("Unknown metric domain name '{}'".format(metric_domain))
            if metric_domain == "project_summary":
                analyze_project_summary(project_name,project_directory,df_metrics)
            elif metric_domain == "duplications":
                analyze_duplications(project_name,project_directory,df_metrics)
            elif metric_domain == "sizes":
                analyze_sizes(project_name,project_directory,df_metrics)
            elif metric_domain == "issues":
                analyze_issues (project_name,project_directory,df_metrics, df_issues)

if __name__ == "__main__":
    main()
