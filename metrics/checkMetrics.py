#
# Copyright (C) 2012-2020 Euclid Science Ground Segment
#
# This library is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser General
# Public License as published by the Free Software Foundation; either version 3.0 of the License, or (at your option)
# any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License along with this library; if not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
#

"""
Display/record :
- the list of all the projects in sonarQube
- the metrics stored in sonarQube for a given projet

For more information:

Python package used:
# https://github.com/kako-nawao/python-sonarqube-api

sonarQube:
# https://docs.sonarqube.org/display/DEV/Web+API
# https://apceuclidccapp.in2p3.fr/sonar/api/metrics/search
# from sonarapi import SonarAPIHandler

Examples:
--------
(...to be completed...)

"""

import argparse
import copy
import json
import os.path
import signal
import sys

import openpyxl  # http://openpyxl.readthedocs.io/en/stable/tutorial.html
import requests  # for API rest requests (package close to the urllib.request package)
from requests.auth import HTTPBasicAuth  # sonarQune log in
from json.decoder import JSONDecodeError

signal.signal(signal.SIGINT, lambda x, y: sys.exit(0))

# disable the security certificate check in Python requests
from urllib3.exceptions import InsecureRequestWarning

requests.packages.urllib3.disable_warnings(category=InsecureRequestWarning)

from yaml import safe_load
import logging.config
import logging.handlers

logger = logging.getLogger(__name__)

from input_output import dictionaries, excel_sheet, file_access

SCRIPT_VERSION = "0.9"

# default fields used to request a sonarQube web service

COMPONENT_FIELDS = ["id",
                    "key",
                    "name",
                    "qualifier",
                    "language",
                    "path",
                    "measures"]

PERIODS_FIELDS = ["previous_version",
                  "previous_analysis",
                  "days"]

# https://docs.sonarqube.org/display/SONAR/Metric+Definitions
#COMPLEXITY_METRICS = ['complexity', 'cognitive_complexity']
COMPLEXITY_METRICS = ['complexity']
DUPLICATION_METRICS = ['duplicated_blocks', 'duplicated_files', 'duplicated_lines', 'duplicated_lines_density']
ISSUE_METRICS = ['violations', 'blocker_violations', 'critical_violations', 'major_violations']
MAINTAINABILITY_METRICS = ['code_smells', 'sqale_rating', 'sqale_index', 'sqale_debt_ratio']
QUALITY_GATE_METRICS = ['alert_status']
RELIABILITY_METRICS = ['bugs', 'reliability_rating', 'reliability_remediation_effort']
SIZE_METRICS = ['classes', 'comment_lines', 'comment_lines_density', 'directories', 'files', 'lines', 'ncloc',
                'functions']
TEST_METRICS = ['branch_coverage', 'conditions_by_line', 'covered_conditions_by_line', 'coverage', 'line_coverage',
                'lines_to_cover', 'skipped_tests', 'uncovered_conditions', 'uncovered_lines', 'tests',
                'test_execution_time',
                'test_errors', 'test_failures', 'test_success_density']

ALL_COMPONENT_METRICS = COMPLEXITY_METRICS + DUPLICATION_METRICS + ISSUE_METRICS + MAINTAINABILITY_METRICS + \
                        QUALITY_GATE_METRICS + RELIABILITY_METRICS + SIZE_METRICS + TEST_METRICS

#COMPONENT_METRICS1 = ['complexity', 'duplicated_lines_density', 'violations', 'code_smells',
#                     'bugs', 'vulnerabilities', 'files', 'lines', 'ncloc', 'comment_lines_density', 'functions']

COMPONENT_METRICS1 = ALL_COMPONENT_METRICS
# pay attention with 'cognitive_complexity' : only after SonarQube 6.0
COMPONENT_METRICS2 = ['complexity', 'cognitive_complexity', 'duplicated_lines_density', 'violations', 'code_smells',
                     'bugs', 'vulnerabilities', 'files', 'lines', 'ncloc', 'comment_lines_density', 'functions']

if sys.platform=='win32':
    HOME_DIR = home = os.path.expanduser('~') # default Home directory
else: # linux system
    HOME_DIR = os.getenv('HOME')

if not HOME_DIR:
    error_message= 'HOME directory not found. Check Environment variable HOME (linux) or HOMEPATH (Windows)'
    logger.critical(error_message)
    raise ValueError(error_message)

CONF_DEFAULT_FILE = os.path.join(HOME_DIR, "sgs_tool_access.json")
#CONF_DEFAULT_FILE = filename_with_full_path("sgs_tool_access.json")


def get_metrics_from_components_data(components_data, fields):
    """
    Return information and metrics from a sonarQube request (containing data on components)

    Parameters
    ----------
    components_data: list of dict
        list of JSON, dict (send by the sonarQube API)
    fields: list of str
        list of field names to retrieve (ie JSON keys to look for value)

    Returns
    -------
    dict [str]:str
        metrics
    """

    fields_and_metrics = []

    for component_data in components_data:
        infos = {}
        for field in fields:
            if field == 'measures':
                # loop over "measures" list (i.e.metrics)
                if 'measures' in component_data.keys():
                    metrics = component_data['measures']
                    for metric in metrics:
                        name = metric['metric']
                        if "_data" not in name:
                            if 'value' in metric:
                                value = metric['value']
                                infos[name] = value
                else:
                    # avoid empty cells if "measures" (i.e. metrics) field not found
                    infos[field] = "x"
            elif field in component_data.keys():
                infos[field] = component_data[field]
            else:
                # avoid empty cells if selected field not found
                infos[field] = "x"
        if infos:
            fields_and_metrics.append(infos)
    return fields_and_metrics


def get_projects_from_Excel(file_name):
    """
    read list of projects from an ExcelSheet file

    Parameters
    ----------
    file_name:str
        sonarQube configuration file (first line = list of projects)

    Returns
    -------
    list of str
        list of projects

    """
    wb = openpyxl.load_workbook(file_name)
    first_sheetname = wb.sheetnames[0]
    ws = wb[first_sheetname]
    # read the first row
    projects = []
    project = {}

    # for row in range(1, first_sheet.nrows):
    #TODO: update row_offset to min and max_row.
    # See https://stackoverflow.com/questions/54617949/in2csv-iter-rows-got-an-unexpected-keyword-argument-row-offset
    # Below id for openpyxl 2.6.4 (not ok after 2.6.0)
    # pip install openpyxl==2.5.9 --user

    import pkg_resources
    # get x.y version of openpyxl
    version_openpyxl = float(pkg_resources.get_distribution("openpyxl").version[0:2])

    if version_openpyxl > 2.5:
        logger.critical('openpyxl should have a version < 2.6.0. Try pip install openpyxl==2.5.9 --user')
        raise ValueError("openpyxl version '{}'".format(version_openpyxl))

    for row_cells in ws.iter_rows(row_offset=1):
        # read first column (sonarqube repository)
        project.clear()
        repository = row_cells[1].value
        if repository:
            project['k'] = repository
            projects.append(copy.deepcopy(project))

    return projects


def get_credentials(config_filename):
    """
    test the connection to the sonarQube server
    Parameters
    ----------
    config_filename:str
        private sonarQube configuration file

    Returns
    -------
    str,str,dict [str]:str
        myUrl, myAuth, proxies : URL, authorization data for another requests and proxy info
    """

    # TODO  see encoding character as in python 2 : open(r'config.json', 'r')
    with open(config_filename, 'r', encoding="utf-8") as f:
        config = json.load(f)

    myUrl = config['sonar']['url']

    if 'password' in config['sonar']:
        muUsername = config['sonar']['username']
        myPassword = config['sonar']['password']
        myHeader = {'user': '{}'.format(muUsername), 'pass': '{}'.format(myPassword)}
    else:
        myToken = config['sonar']['private_token']
        myHeader = {'Authorization': 'token {}'.format(myToken)}

    if 'https' in config['sonar']:
        https = config['sonar']['https']
        proxies = {'https': https}
    else:
        proxies = None

    return myUrl, myHeader, proxies


def get_data_from_request_in_sonarQube(config_filename, sonar_api, api_parameters):
    """
    run a sonarQube request and retrieve response (exception request management)
    Parameters
    ----------
    config_filename:str
        private sonarQube configuration file
    sonar_api:str
        api sonar address
    api_parameters:dict
        api sonar parameters

    Returns
    -------
    dict[str]:str

    """
    # connect to the sonarQube server
    myUrl, myHeader, proxies = get_credentials(config_filename)
    myAuth = None

    # TO DO avoid user/password connection
    if 'user' in myHeader:
        myAuth = HTTPBasicAuth(myHeader['user'], myHeader['pass'])

    repeat = True
    while repeat:
        try:
            repeat = False
            logger.debug("Trying request {}".format(myUrl + sonar_api))
            if myAuth:
                response = requests.get(myUrl + sonar_api,
                                        auth=myAuth,
                                        params=api_parameters,
                                        proxies=proxies,
                                        verify=False, stream=True, timeout=10000)

            else:
                response = requests.get(myUrl + sonar_api,
                                        headers=myHeader,
                                        params=api_parameters, proxies=proxies,
                                        verify=False, stream=True, timeout=10000)
        # requests.exceptions.ConnectionError
        except requests.exceptions.ChunkedEncodingError:
            logger.warning('chunked_encoding_error happened')
            repeat = True
            continue
        except requests.exceptions.ProxyError:
            logger.warning('ProxyError happened')
            repeat = True
            continue
        except requests.exceptions.ConnectionError:
            logger.error('connection_error happened')
            pass
        except Exception as e:
            logger.critical('exception ocurred')
            raise e

        # build and run the request
        try:
            data = response.json()

        except requests.exceptions.ChunkedEncodingError:
            logger.warning('chunked_encoding_error happened on decoding response (repeat the operation...)')
            repeat = True

        except requests.exceptions.ProxyError:
            logger.warning('ProxyError happened (repeat the operation...)')
            repeat = True

        except JSONDecodeError:
            logger.warning('JSONDecodeError happened (repeat the operation...)')
            repeat = True

    if 'err_code' in data:
        raise ValueError(
            "Request '{}' terminated with this message: '{}'".format(myUrl + sonar_api, data))

    if 'errors' in data:
        raise ValueError(
            "Request '{}' terminated with this message: '{}'".format(myUrl + sonar_api, data))
    return data


def try_connexion_to_sonarQube(config_filename):
    """
    raise error if no connexion possible

    Parameters
    ----------
    config_filename:str
        private sonarQube configuration file

    """
    # build and run the request
    sonar_api = 'api/authentication/validate'
    api_parameters = {}
    data = get_data_from_request_in_sonarQube(config_filename, sonar_api, api_parameters)


def get_projects_in_sonarQube(config_filename):
    """
    returns all the projects in sonarQube

    Parameters
    ----------
    config_filename:str
        private sonarQube configuration file

    Returns
    -------
    dict[str]:str
        sonarQube projects (<sonar URL>/sonar/web_api/api/projects)

    """
    # build and run the request
    sonar_api = 'api/projects/index'
    # api_parameters = {'desc':'true'}
    api_parameters = {}
    projects = get_data_from_request_in_sonarQube(config_filename, sonar_api, api_parameters)

    return projects


def get_periods_of_a_project(config_filename, project_key):
    """
    returns all the component periods in sonarQube

    Parameters
    ----------
    config_filename:str
        private sonarQube configuration file
    project_key:str
        sonar project key

    Returns
    -------
    dict[str]:str
        sonarQube periods (<sonar URL>/sonar/web_api/measures/component)
    """

    # build and run the request (nloc metric is taken only to get periods values
    periods = ""
    sonar_api = 'api/measures/component'
    api_parameters = {'componentKey': project_key, 'additionalFields': 'periods', 'metricKeys': 'ncloc'}
    project_data = get_data_from_request_in_sonarQube(config_filename, sonar_api, api_parameters)

    if 'periods' in project_data:
        periods = project_data['periods']
    return periods


def get_projects(config_filename, pattern=None, add_periods=None):
    """
    returns all the projects in sonarQube

    Parameters
    ----------
    config_filename:str
        private sonarQube configuration file
    pattern:str
        str used to select project (selection: "if <pattern< in <project>...")
    add_periods:bool
        add sonar periods in projects information

    Returns
    -------
    dict[str]:str
        sonarQube periods ( <sonar URL>/sonar/web_api/api/projects)
    """

    # build and run the request
    sonar_api = 'api/projects/index'
    # TODO: parameters below do not work !? 
    # api_parameters = {'desc':'yes'}
    # it works in the browser ! :
    # https://codeen-app.euclid-ec.org/sonar/api/projects/index?key=LE3_SELID_develop-SEL_ID.git&desc=true
    # api_parameters = {'key':'LE3_SELID_develop-SEL_ID.git','desc':'yes'}

    api_parameters = {}
    list_of_projects = get_data_from_request_in_sonarQube(config_filename, sonar_api, api_parameters)

    # add periods in each product (i.e row)
    if add_periods:
        list_projects_with_periods = []
        for dict_of_project_values in list_of_projects:

            project_key = dict_of_project_values['k']
            logger.debug('sonarQube project repository found: {}'.format(project_key))
            list_of_periods_values = get_periods_of_a_project(config_filename, project_key)

            for dict_of_periods_values in list_of_periods_values:
                if 'mode' in dict_of_periods_values:
                    if dict_of_periods_values['mode'] == 'previous_analysis':
                        dict_of_project_values['previous_analysis'] = dict_of_periods_values['date']
                    if dict_of_periods_values['mode'] == 'previous_version':
                        dict_of_project_values['previous_version'] = dict_of_periods_values['date']
                    if dict_of_periods_values['mode'] == 'days':
                        dict_of_project_values['current_version'] = dict_of_periods_values['date']
            list_projects_with_periods.append(dict_of_project_values)
        list_of_projects = list_projects_with_periods

    # select list of project names using the pattern (<> "*")
    if pattern:
        list_projects_with_pattern = []
        for dict_of_project_values in list_of_projects:
            project_name = dict_of_project_values['k']
            if pattern in project_name:
                list_projects_with_pattern.append(dict_of_project_values)
        list_of_projects = list_projects_with_pattern

    return list_of_projects


def get_domains_in_sonarQube(config_filename):
    """
    returns all the domains in sonarQube

    Parameters
    ----------
    config_filename:str
        private sonarQube configuration file

    Returns
    -------
    list of str
        sonarQube metric domains ( <sonar URL>/api/metrics/domains)
    """

    # build and run the request
    sonar_api = 'api/metrics/domains'
    api_parameters = {}
    domains = get_data_from_request_in_sonarQube(config_filename, sonar_api, api_parameters)

    return domains


def get_sonar_status_and_version(config_filename):
    """
    returns the version and status of sonarQube server

    Parameters
    ----------
    config_filename:str
        private sonarQube configuration file

    Returns
    -------
    str
        sonarQube server version and status
    """

    # build and run the request
    sonar_api = 'api/system/status'
    api_parameters = {}
    project_data = get_data_from_request_in_sonarQube(config_filename, sonar_api, api_parameters)

    version = project_data['version']
    status = project_data['status']

    return version, status


def get_sonar_version(config_filename):
    # return version in float (3 first digits)
    version, status = get_sonar_status_and_version(config_filename)
    return float(version[:3])


def get_sonar_plugins(config_filename):
    """
    returns the version and status of sonarQube server

    Parameters
    ----------
    config_filename:str
        private sonarQube configuration file

    Returns
    -------
    str
        list of the plugins installed
    """

    # build and run the request
    #sonar_api = 'api/plugins/available'
    sonar_api = 'api/plugins/installed'

    api_parameters = {}
    project_data = get_data_from_request_in_sonarQube(config_filename, sonar_api, api_parameters)

    sonar_plugins = project_data['plugins'] # list of dictionaries which keys are 'key','name','description",...
    return sonar_plugins

def get_sonar_profiles(config_filename):
    """
    returns the profiles in sonarQube server

    Parameters
    ----------
    config_filename:str
        private sonarQube configuration file

    Returns
    -------
    str
        list of the profiles installed
    """

    # build and run the request
    #sonar_api = 'api/plugins/available'
    sonar_api = 'api/qualityprofiles/search'

    api_parameters = {}
    project_data = get_data_from_request_in_sonarQube(config_filename, sonar_api, api_parameters)

    sonar_profiles = project_data['profiles'] # list of dictionaries which keys are 'key','name','description",...
    return sonar_profiles

def backup_xml_profile(config_filename):
    """
    get the profile file and save it in XML file
    TODO: avoid JSON decode task (XML file downloaded...)
    :param config_filename: rivate sonarQube configuration file
    :return:    """
    profiles = get_sonar_profiles(config_filename)
    logger.info("sonarQube server: list of the profiles:")
    for profile in profiles:
        key = profile["key"]
        name = profile["name"]
        count_active_rules = profile["activeRuleCount"]
        isDefault= profile["isDefault"]

        logger.info("Backup Quality profile: Key:{} : Name:{}: Is default:{} : #Active rules: {}".format(key,name,isDefault,count_active_rules))
        sonar_api = 'api/qualityprofiles/backup'
        api_parameters  = {'profileKey': key} # not qualityProfile as documented !!

        # avoid JSON decode task (XML file downloaded...)
        # JSONDecodeError happened (repeat the operation...)
        # try curl -v -u admin:admin "http://localhost:9000/api/qualityprofiles/backup?profileKey=AW70gvtp2DiPsqZ1roxC"
        #project_data = get_data_from_request_in_sonarQube(config_filename, sonar_api, api_parameters)

def get_metrics_available_in_sonarQube(config_filename):
    """
    returns sonarQube metrics available

    Parameters
    ----------
    config_filename:str
        private sonarQube configuration file

    Returns
    -------
    list of str
        sonarQube metric domains ( <<sonar URL>/sonar/web_api/api/metrics)
    """

    # build and run the request

    # domains = get_domains_in_sonarQube (config_filename)
    # domains_str = ",".join(domains['domains'])
    sonar_api = 'api/metrics/search'
    nb_metrics_read = 0
    page = 1
    total_metrics_to_read = 1
    metrics_available = []

    while nb_metrics_read < total_metrics_to_read:
        api_parameters = {'f': 'name,description,domain', 'p': page}
        project_data = get_data_from_request_in_sonarQube(config_filename, sonar_api, api_parameters)

        # page = project_data['p']
        page_size = project_data['ps']
        total_metrics_to_read = project_data['total']
        metrics_available += project_data['metrics']

        nb_metrics_read += len(project_data['metrics'])
        page += 1

    return metrics_available


def write_sonarQube_data(sonarQube_data, quiet, file_name, section_name="sonarQube data"):
    """
    write data from API requests on the screen or in a output file

    Parameters
    ----------
    sonarQube_data: dict [str]:str
        data from sonar API requests
    quiet: bool
        quiet mode (i.e. no display on screen) if true
    file_name:str
        output filename (.xlsx,.json,...)
    section_name
        if file is spreadsheet, name of the sheet

    """

    # copy data into a compliant container (list of dictionaries)
    listofdict = dictionaries.Dictionaries()
    listofdict.body = sonarQube_data
    if file_name:
        if file_name.endswith(".xlsx"):
            spreadsheet = excel_sheet.ExcelSheet(file_name, listofdict)
            spreadsheet.write_sheet(sheet_name=section_name)
            spreadsheet.save_and_close()

        if file_name.endswith(".json"):
            listofdict.write_json(file_name)
        logger.info("\nOutfile generated: {}".format(file_name))

    if not quiet:
        listofdict.write_screen()
        # logger.info("Writing sonarQube data on the screen")


def get_metrics_of_a_project(config_filename, project_key, metrics=None):
    """
    returns all the projects in sonarQube

    Parameters
    ----------
     config_filename:str
        private sonarQube configuration file
    project_key:str
        sonar project key    metrics
    metric: None or list of str
        list of selected metrics. If set to None, specific metrics will be used
    Returns
    -------
    dict[str]:str
        sonarQube periods (<sonar URL>/sonar/web_api/measures/component)
    """

    # If metrics set to None, all the available metrics will be used
    if metrics is None:
        metrics_component_str = ",".join(COMPONENT_METRICS)

    # build and run the request (nloc metric is taken only to get periods values
    sonar_api = 'api/measures/component'
    api_parameters = {'componentKey': project_key, 'metricKeys': metrics_component_str}
    project_data = get_data_from_request_in_sonarQube(config_filename, sonar_api, api_parameters)

    periods = project_data['metrics']
    return periods


def get_metrics_in_sonarQube(config_filename, sonarQube_project, metrics_list=None):
    """
    get all the selected metrics from a sonarQube project

    Parameters
    ----------
     config_filename:str
        private sonarQube configuration file
    sonarQube_project:str
        an existing project in sonarQube
    metrics_list:list of str or None
        list of selected metrics. If set to None, all the available metrics will be used

    Returns
    -------
    dict[str]:str
        onarQube metrics (<sonar URL>/sonar/web_api/api/measures/component_tree)
    """

    # If metrics set to None, all the available metrics will be used
    if metrics_list is None:
        metrics_list = []
        metrics_available = get_metrics_available_in_sonarQube(config_filename)
        for metric in metrics_available:
            metric_key = metric['key']
            # TODO : check bug in sonarQube with metric_key == 'development_cost'
            if metric_key != 'development_cost':
                metrics_list += [metric['key']]
            else:
                pass
    metrics_str = ",".join(metrics_list)
    # metrics_component_str = ",".join(COMPONENT_METRICS)

    # build and run the request
    sonar_api = 'api/measures/component_tree'
    api_parameters = {'baseComponentKey': sonarQube_project, 'metricKeys': metrics_str,
                      'additionalFields': 'periods'}
    project_data = get_data_from_request_in_sonarQube(config_filename, sonar_api, api_parameters)

    # header information
    # TODO: manages pages !!

    paging = project_data['paging']
    baseComponent = project_data['baseComponent']
    baseComponent_list = []
    baseComponent_list.append(baseComponent)
    qualifier = baseComponent['qualifier']
    measures = baseComponent['measures']
    components_data = project_data['components']

    list_of_metrics_components = get_metrics_from_components_data(components_data, COMPONENT_FIELDS)
    list_of_metrics_baseComponent = get_metrics_from_components_data(baseComponent_list, COMPONENT_FIELDS)

    metrics_in_sonarQube = list_of_metrics_components + list_of_metrics_baseComponent
    return metrics_in_sonarQube


def get_rules_in_sonarQube(config_filename, quality_profile_key):
    """
    returns sonarQube rules available

    Parameters
    ----------
    config_filename:str
        private sonarQube configuration file
    languages:str (words separated with comma, as 'c++,py')
        languages available in sonarQube: c++,py,java,...

    Returns
    -------
    list of str
        sonarQube rule domains ( <<sonar URL>/sonar/web_api/api/rules/search)
    """

    # build and run the request

    # domains = get_domains_in_sonarQube (config_filename)
    # domains_str = ",".join(domains['domains'])
    sonar_api = 'api/rules/search'
    nb_rules_read = 0
    page = 1
    page_size = 500
    nb_rules_to_read = 1
    rules_available = []
    rules_in_quality_profile = []

    while nb_rules_read < nb_rules_to_read:
        # api_parameters = {'languages':languages,'f': 'repo,name', 'p': page,'ps':page_size}
        # api_parameters = {'languages': languages, 'p': page, 'ps': page_size}
        # TODO: add params after status below
        api_parameters = {'activation':'yes','activation':'true','qprofile':quality_profile_key, 'f': 'repo,name,severity,status,params,actives', 'p': page, 'ps': page_size}

        rules_data = get_data_from_request_in_sonarQube(config_filename, sonar_api, api_parameters)
        # page = project_data['p']
        page_size = rules_data['ps']
        nb_rules_to_read = rules_data['total']

        no_rule = 0
        for rule in rules_data['rules']:
            key_rule = str(rule['key'])
            severity_quality_profile_key = rules_data['actives'][key_rule][0]['severity']
            params_quality_profile_key = rules_data['actives'][key_rule][0]['params']
            rules_data['rules'][no_rule]['profile_severity']=severity_quality_profile_key
            rules_data['rules'][no_rule]['profile_params']=params_quality_profile_key
            no_rule+=1

        rules_available += rules_data['rules']
        nb_rules_read += len(rules_data['rules'])
        page += 1

    return rules_available


def get_issues_in_sonarQube(config_filename, sonarQube_project):
    """
    get all the issues from a sonarQube project

    Parameters
    ----------
     config_filename:str
        private sonarQube configuration file
    sonarQube_project: str
        an existing project in sonarQube

    Returns
    -------
    dict[str]:str
        sonarQube issues (<sonar URL>/sonar/web_api/api/issues/search)
    """

    issues = []

    # build and run the request
    sonar_api = 'api/issues/search'
    api_parameters = {'componentKeys': sonarQube_project, 'severities': 'MAJOR,CRITICAL,BLOCKER'}
    project_data = get_data_from_request_in_sonarQube(config_filename, sonar_api, api_parameters)

    # header information
    # TODO: manages pages !!
    paging = project_data['paging']
    issues = project_data['issues']

    selected_issues = [];
    # delete non important keys
    for issue in issues:

        for key in ['flows', 'tags', 'textRange', 'comments', 'attr', 'transitions', 'actions']:
            if key in issue:
                issue.pop(key, None)
        selected_issues.append(issue)

    return selected_issues


def construct_parser():
    parser = argparse.ArgumentParser()

    parser.add_argument("-j",
                        "--config",
                        action="store",
                        metavar="FILE",
                        default=CONF_DEFAULT_FILE,
                        help="The private sonarQube configuration file, default is " + CONF_DEFAULT_FILE)

    parser.add_argument("-m",
                        "--metrics",
                        action="store",
                        nargs="*",
                        metavar="NAME",
                        default="all",
                        help="A space separated list of metrics names to be stored in the output (default all)")

    parser.add_argument("-i",
                        "--issues",
                        action="store_true",
                        help="list all issues in sonarQube (from Major severity)")

    parser.add_argument("-l",
                        "--show_projects",
                        action="store_true",
                        help="list all the projects in sonarQube")

    parser.add_argument("-q",
                        "--quiet",
                        action="store_true",
                        help="no output on the screen")

    parser.add_argument("-p",
                        "--project",
                        action="store",
                        metavar="NAME",
                        # default='VIS',
                        default='EL_SpectrumLib_develop-EL_SpectrumLib.git',
                        help="project or component in sonarQube")

    parser.add_argument("-o",
                        "--output",
                        action="store",
                        metavar="FILE",
                        default="sonarQube_data.xlsx",
                        help="ExcelSheet file name for output (default='sonarQube_data.xlsx')")

    parser.add_argument("--show_metrics",
                        action="store_true",
                        help="Print out the list of selected metric names.")

    parser.add_argument("--backup_xml_profile",
                        action="store_true",
                        help="Backup a quality profile in XML form")

    parser.add_argument("--show_sonar_plugins",
                        action="store_true",
                        help="Print out the list of the plugins")

    parser.add_argument("--show_sonar_profiles",
                        action="store_true",
                        help="Print out the list of the profiles")

    parser.add_argument("--quality_profile_key",
                        action="store",
                        metavar="NAME",
                        help="Key of the quality profile in sonarQube. Use --show_sonar_profiles before")

    parser.add_argument("--show_sonar_version",
                        action="store_true",
                        help="Print the sonarQube server version.")

    parser.add_argument("--show_all_metrics",
                        action="store_true",
                        help="Print out the list of all possible metric names available in sonarQube")

    parser.add_argument("--show_sonar_rules",
                        action="store_true",
                        help="Print out the list of all C++ and Python rules available in sonarQube")

    parser.add_argument("-v",
                        "--version",
                        action="store_true",
                        help="Print the version of this script")

    return parser


def main(args=None):

    """The main routine."""
    if args is None:
        args = sys.argv[1:]

    log_file = file_access.filename_with_full_path(__file__,"logging.yaml")

    try:
        with open(log_file) as handle:
            log_cfg = safe_load(handle)
    except Exception as e:
        raise ValueError('Could not load logging configuration file: {0}'.format(e))

    logging.config.dictConfig(log_cfg)
    # create (root) logger
    logger = logging.getLogger()
    # logger.setLevel(logging.DEBUG)
    logger.debug("START __main__")

    parser = construct_parser()
    args = parser.parse_args()

    try_connexion_to_sonarQube(args.config)

    # load default list of metrics vs sonarqube version
    version = get_sonar_version(args.config)
    if version < 6.0:
        COMPONENT_METRICS = COMPONENT_METRICS1
    else:
        COMPONENT_METRICS = COMPONENT_METRICS2

    if args.show_metrics:
        logger.info(COMPONENT_METRICS)

    elif args.show_sonar_version:
        version, status = get_sonar_status_and_version(args.config)
        logger.info("sonarQube server version: {} (status: {})".format(version, status))

    elif args.version:
        logger.info("version {}".format(SCRIPT_VERSION))

    elif args.show_sonar_plugins:
        plugins = get_sonar_plugins(args.config)
        logger.info("sonarQube server: list of the plugins installed :")
        for plugin in plugins:
            if version < 6.0:
                logger.info("{}:{}:{} - Organization:{}".format(plugin["name"], plugin["version"],  plugin["description"], plugin["organizationName"]))
            else:
                logger.info ("{}:{}:{} - Organization:{} - Filename:{}".format(plugin["name"],plugin["version"],plugin["description"],plugin["organizationName"],plugin["filename"]))

    elif args.show_sonar_profiles:
        profiles = get_sonar_profiles (args.config)
        logger.info("sonarQube server: list of the profiles:")
        for profile in profiles:
            logger.info ("Key:{} : Name:{}: Is default:{} : #Active rules: {}".format(profile["key"],profile["name"],profile["isDefault"],profile["activeRuleCount"]))

    elif args.backup_xml_profile:
        backup_xml_profile(args.config)

    elif args.show_all_metrics:
        metrics_available = get_metrics_available_in_sonarQube(args.config);
        write_sonarQube_data(metrics_available, args.quiet, args.output, section_name="Metrics Definitions")

    elif args.show_sonar_rules:
        rules_available = get_rules_in_sonarQube(args.config, quality_profile_key=args.quality_profile_key);
        write_sonarQube_data(rules_available, args.quiet, args.output, section_name="Python Rules")
        #write_sonarQube_data(rules_available, args.quiet, "", section_name="Python Rules")

    elif args.show_projects:
        projects = get_projects(args.config, pattern=None, add_periods=True)
        write_sonarQube_data(projects, args.quiet, args.output, section_name="sonarQube projects")

    else:

        if args.metrics is not None:

            if args.metrics == "all":
                args.metrics = COMPONENT_METRICS
            else:  # check if each metric done in argument exists in SonarQube
                for col_name in args.metrics:
                    if not col_name in ALL_COMPONENT_METRICS:
                        raise ValueError("Unknown metric name '{}'")
        if args.project:

            if os.path.exists(args.project):
                projects = get_projects_from_Excel(args.project)
            else:
                projects = get_projects(args.config, pattern=args.project, add_periods=False)

            data = []
            if projects:
                for project in projects:
                    project_key = project['k']
                    if project_key:
                        logger.info('sonarQube project repository : {}'.format(project_key))

                    if args.issues:
                        data += get_issues_in_sonarQube(args.config, project_key)
                    else:
                        data += get_metrics_in_sonarQube(args.config, project_key, metrics_list=args.metrics)
                write_sonarQube_data(data, args.quiet, args.output, section_name="values")

if __name__ == "__main__":
    main()



